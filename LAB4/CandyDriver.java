/** A driver program to show the functionality of the Candy class.
 *      This program loads info from candy.txt to create candy objects and prints
 *      the results.
 * 
 *  There wasn't an attempt to protect the program from improper input as the requirements
 *   did not specify a markup languages or format (such as CSV or XML) so it's only guarunteed
 *   to work where each candy is 4 lines, and each line is as follows
 * 
 *      type
 *      name
 *      pricePerPound
 *      {NEWLINE}
 * 
 *   errant input will result in improper data or eventually be detected due to
 *   improper type exceptions from parseDouble
 * 
 * @author Brian J Stout
 */

import java.util.Scanner;
import java.io.*;

class CandyDriver
{
    private static final String FILENAME = "candy.txt";
    public static void main (String[] args) throws IOException
    {
        File file = new File(CandyDriver.FILENAME);
        if(!file.exists() || !file.isFile())
        {
            System.out.println("ERROR: candy.txt not found!");
            System.exit(0);
        }
        Scanner inputFile = new Scanner(file);

        // Parses through the entire file till the endw
        while(inputFile.hasNext())
        {
            Candy candy = parseRecord(inputFile);
            System.out.println(candy);
        }
    }

    /** A record in candy text is formatted as such:
     *      type
     *      name
     *      pricePerPound
     *      {blank line}
     * 
     * parseRecord() Will read 4 lines and create a candy object based on a provided 4 line record.
     * 
     * @param inputFile a Scanner object of the file being read for input
     * @return An instance of a Candy object
     */
    public static Candy parseRecord(Scanner inputFile)
    {
        String line = "";
        String name = "";
        String type = "";
        String pricePerPound = "";

        for (int i = 0; i < 4; i++)
        {
            // If the input file doesn't have anymore input quit
            if(inputFile.hasNext() == false)
            {
                break;
            }

            // Shouldn't be nessecary but inputFile may be bugged or contain not String
            //  formattable data
            try {
                line = inputFile.nextLine();
            } catch (Exception e)
            {
                System.out.println("Input File contains invalid input or is improperly formatted");
                System.out.println(e);
                System.exit(0);
            }

            // The provided text document includes and EOF file marker in plain text so
            //  we check for that
            if(line.equals("EOF"))
            {
                break;
            }

            // Without using an array to index through a switch is required to
            //  assign the right values to later be converted to the candy object
            switch(i)
            {
                case 0:
                    type = line;
                    break;
                case 1:
                    name = line;
                    break;
                case 2:
                    pricePerPound = line;
                    break;
                // This case should be an empty line so move along
                case 3:
                    break;
            }
        }

        // Create the candy object and return it to main
        Candy candy = new Candy(type, name, parseDouble(pricePerPound));
        return candy;
    }

    /** Will attempt to convert a string into a double and exit the program
     *   if the string cannot be converted.
     * 
     * @param userInput the string being converted
     * @return An equivalent double to it's string representation
     */
    public static Double parseDouble(String userInput)
    {
        double returnDouble = 0.0;

        try {
            returnDouble = Double.parseDouble(userInput);
        } catch (Exception e) {
            System.out.println("You didn't enter a valid number!  Exiting...");
            System.exit(0);
        }   
        return returnDouble;  
    }
}