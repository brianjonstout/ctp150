/** Class which represents a type of candy that might be sold at a store.
 *      It includes the Type of the candy, the name of it, and it's price.
 * 
 * 
 * @author Brian J Stout
 */

class Candy
{
    private String type;
    private String name;
    private double pricePerPound;
    private static final double LOW_PRICE_ITEM = 1.25;
    private static final double HIGH_PURCHASE_ITEM = 2.5;

    /** Default constructor for Candy that gives blank strings and 0 values
     * 
     * @return An instance of a candy object
     */
    public Candy()
    {
        type = "";
        name = "";
        pricePerPound = 0;
    }

    /** Returns an instance of the candy object with all the values initialized
     * 
     * @param startType the type of the candy, such as Chocolate or Taffy
     * @param startName the name of the candy, such as Hersey's
     * @param startPricePerPound the cost per pound of the candy
     * @return An instance of a candy object
     */
    public Candy(String startType, String startName, double startPricePerPound)
    {
        type = startType;
        name = startName;
        pricePerPound = startPricePerPound;
    }

    /** Getter for type attribute
     * 
     * @return type
     */
    public String getType()
    {
        return type;
    }

    /** Setter for type
     * 
     * @param newType the new type value
     */
    public void setType(String newType)
    {
        type = newType;
    }

    /** Getter for name attribute
     * 
     * @return the name of the candy
     */
    public String getName()
    {
        return name;
    }

    /** Setter for name attribute
     * 
     * @param newName the new name of the candy
     */
    public void setName(String newName)
    {
        name = newName;
    }

    /** Getter for pricerPerPound attribute
     * 
     * @return the price per pound of the candy
     */
    public double getPricePerPound()
    {
        return pricePerPound;
    }

    /** Setter for pricePerPound attribute 
     * 
     * @param newPricePerPound the new price per pound of the candy
     */
    public void setPricePerPound(double newPricePerPound)
    {
        pricePerPound = newPricePerPound;
    }

    /** A getter for the private constant value of what is considered a "Low Price Candy"
     * 
     * @return The constant LOW_PRICE_ITEM
     */
    public static double getLOW_PRICE_ITEM()
    {
        return LOW_PRICE_ITEM;
    }

    /** A getter for the private constant value of what is considered a "High Purchase Item"
     * 
     * @return the constant HIGH_PURCHASE_ITEM
     */
    public static double getHIGH_PURCHASE_ITEM()
    {
        return HIGH_PURCHASE_ITEM;
    }

    /** Returns the total price of the candy provided a pound amount
     * 
     * @param pounds the amount in pounds of candy being purchased
     * @return the price of purchasing that many pounds of candy
     */
    public double getTotalPurchase(double pounds)
    {
        return pricePerPound * pounds;
    }

    /** Converts the candy object into a formatted String
     *  @return A string representing the candy object
     */
    public String toString()
    {
        String retString = "";
        retString += String.format("Name: %s, Type: %s.", this.name, this.type);

        if (pricePerPound <= LOW_PRICE_ITEM)
        {
            retString += " This is a low priced item.";
        }
        else if (pricePerPound > HIGH_PURCHASE_ITEM)
        {
            retString += " This is a high priced item.";
        }

        return retString; 
    }
}