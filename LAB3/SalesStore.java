/** This program reads the input of all the stores total sales and formats them in a table to output to the user
 * 
 * The NUMBEROFSTORES value will increase the rows of the table
 * The NUMBEROFDAYS value will increase the columns of a table, and will be correctly formatted as long as
 *   the terminal window is wide enough to represent all the rows characters (including spaces)
 * 
 * TABLELENGTH value is the width of each column, if the length of a column is greater than TABLELENGTH it will ruin the formatting
 * 
 * When testing please decrease the NUMBEROFSTORES and NUMBEROFDAYS to decrease the number of user input so you can get
 *   through the program loop quicker
 * 
 * @author Brian J Stout
 */

import java.util.Scanner;

 class SalesStore
 {
    private static final int NUMBEROFSTORES = 3;
    private static final int NUMBEROFDAYS = 5;
    private static final int TABLELENGTH = 20;

    private static Double TotalSales = 0D;

    // Using global here since there is only one type of input (Asking for user string) and it makes sense
    private static Scanner scan = null;

    public static void main(String[] args)
    {
        if (SalesStore.NUMBEROFSTORES < 1 || SalesStore.NUMBEROFDAYS < 1)
        {
            System.out.println("Can't except data for no stores or no days!  Exiting...");
            System.exit(0);
        }

        SalesStore.scan = new Scanner(System.in);
        String outputString = "";

        for (int i = 0; i < SalesStore.NUMBEROFSTORES; i++)
        {
            outputString += returnRow(i+1);
        }

        System.out.print(returnHeader());
        System.out.print(outputString);
        System.out.print(returnFooter());

        SalesStore.scan.close();
    }

    /**
     * Grabs a number (ignoring $ characters) and runs it into parseDouble returning a double value from the input
     * 
     * @param scan
     * @return a double equivalent to the userInput string representing it
     */
    public static Double retrieveSalesNumber(Scanner scan)
    {
        String userInput = scan.nextLine();
        userInput = userInput.replace("$", "");
        Double returnDouble = parseDouble(userInput);

        return returnDouble;
    }

    /**
     * Will verify that the userInput can be succesfully parsed into a double, and if it can't will exit the program
     * 
     * @param userInput
     * @return a double equal to the string representation
     */
    public static Double parseDouble(String userInput)
    {
        double returnDouble = 0.0;

        try {
            returnDouble = Double.parseDouble(userInput);
        } catch (Exception e) {
            System.out.println("You didn't enter a valid number!  Exiting...");
            System.exit(0);
        }   
        return returnDouble;  
    }

    /**
     * This generates a top line of each day of the week, for any number of columns.
     * 
     * @return A string of the days of the week per column EX: "Monday    Tuesday    Wednesday    Thursday    Friday"
     */
    public static String returnHeader()
    {
        // Add a left aligned variable with a default space of TABLELENGTH to align with rest of table
        // Credit: Method of using .format to do variable sized formatted is adapted from https://www.geeksforgeeks.org/java-string-format-examples/
        String formatString = "%-" + SalesStore.TABLELENGTH + "s";
        String header = "";

        header += String.format(formatString, "");

        for(int i = 0; i < SalesStore.NUMBEROFDAYS; i++)
        {
            int day = i % 7;

            switch(day)
            {
                // Append each date based on the NUMBEROFDAYS, but if NUMBEROFDAYS is larger than Number of days in a normal week reset back to Monday
                // I Know the project is just 5 days but I kind of did this just for fun
                case 0:
                    header += String.format(formatString, "Monday");
                    break;
                case 1:
                    header += String.format(formatString, "Tuesday");
                    break;
                case 2:
                    header += String.format(formatString, "Wednesday");
                    break;
                case 3:
                    header += String.format(formatString, "Thursday");
                    break;
                case 4:
                    header += String.format(formatString, "Friday");
                    break;
                case 5:
                    header += String.format(formatString, "Saturday");
                    break;
                case 6:
                    header += String.format(formatString, "Sunday");
                    break;
            }
        }

        header += String.format(formatString, "Weekly Sales");
        return header += "\n";
    }

    /**
     * Generates a single string for a row of a stores sales, spaces correctly for the tables
     * 
     * @param storeNumber what store number should be in the first column of the table
     * @return a string with the correct amount of spaces to line it up with the table
     */
    public static String returnRow(int storeNumber)
    {
        String formatString = "%-" + SalesStore.TABLELENGTH + "s";
        String formatDouble = "%-" + SalesStore.TABLELENGTH + ".2f";
        String row = "";
        String storeName = String.format("Store %d", storeNumber);
        row += String.format(formatString, storeName);

        Double weeklySales = 0D;

        for (int i = 0; i < SalesStore.NUMBEROFDAYS; i++)
        {
            System.out.print("Enter " + returnDay(i) + " sales for " + storeName + " : ");
            Double salesNumber = retrieveSalesNumber(SalesStore.scan);
            weeklySales += salesNumber;
            row += String.format(formatDouble, salesNumber);
        }

        row += String.format("$" + formatDouble, weeklySales);
        SalesStore.TotalSales += weeklySales;

        return row += "\n";
    }

    /** 
     * When given a day it will return a string based on 0 for which day of the week it is (0 being Monday)
     *  numbers over 6 it'll switch back to Monday
     * 
     * @param day the index of the day of the week you want returned, 0 is Monday, 6 is Sunday
     * @return a string representation of the day of the week
     */
    public static String returnDay(int day)
    {
        String returnString = "";

        switch(day % 7)
        {
            case 0:
                returnString = "Monday";
                break;
            case 1:
                returnString = "Tuesday";
                break;
            case 2:
                returnString = "Wednesday";
                break;
            case 3:
                returnString = "Thursday";
                break;
            case 4:
                returnString = "Friday";
                break;
            case 5:
                returnString = "Saturday";
                break;
            case 6:
                returnString = "Sunday";
                break;
        }

        return returnString;
    }

    /**
     * Creates a formatted string that places "Total Sales: %f" at the correct distance at the bottom
     *  of the table so it's lined up correctly
     * 
     * @return a string spaced correctly displaying the total sales of all the stores
     */
    public static String returnFooter()
    {
        int emptySpaces = SalesStore.NUMBEROFDAYS;
        emptySpaces *= SalesStore.TABLELENGTH;

        String formatEmptySpaces = "%" + emptySpaces + "s";
        String formatString = "%-" + SalesStore.TABLELENGTH + "s";
        String formatDouble = "$%-" + SalesStore.TABLELENGTH + ".2f";

        String footer = "";
        footer += String.format(formatEmptySpaces, "");

        footer += String.format(formatString, "Total Sales: ");
        footer += String.format(formatDouble, SalesStore.TotalSales);

        return footer;
    }
 }