/**
 * This program will determine the highest temperature, the average temperate, and the number of months that average more than 60 degrees from temperatures
 *   read in a file
 * 
 * You can set which file the program will read with AnnapolisTemperatures.FILENAME
 * If FILENAME doesn't exist the program will create one with dummy data
 * 
 * @author Brian J Stout
 */

import java.util.Scanner;
import java.io.*;
import java.time.Month; // Credit: https://www.tutorialspoint.com/javatime/javatime_month_of.htm

 class AnnapolisTemperatures
 {
    private static final String FILENAME = "annapolis.txt";
    private static final int MONTHSINYEAR = 12;

     public static void main(String[] args) throws IOException
     {

        File file = new File(AnnapolisTemperatures.FILENAME);
        // If the file doesn't exist create one
        if (!file.exists())
        {
            PrintWriter outputFile = new PrintWriter(AnnapolisTemperatures.FILENAME);
            outputFile.print("44\n48\n57\n68\n77\n86\n89\n87\n81\n69\n59\n48");
            outputFile.close();
        }

        // If the system is unable to open file for whatever reason print out error and the exception responsible
        Scanner inputFile = null;
        try{
            inputFile = new Scanner(file);
        }catch (Exception e) {
            System.out.printf("Unable to write or open a file: %s\n", e);
            System.exit(0);
        }


        Double totalCount = 0D;

        int highestTemperature = 0;
        String highestMonth = "";

        int numberOfMonthsAbove60 = 0;
        String monthsAbove60 = "";

        for(int i = 0; i < AnnapolisTemperatures.MONTHSINYEAR; i++)
        {

            // If the file being read from doesn't have enough entries break out to avoid exceptions
            if(inputFile.hasNext() == false)
            {
                break;
            }

            int temperature = 0;

            // Trys to convert text input into an Int, but will exit if the input is invalid
            try {
                temperature = inputFile.nextInt();
            } catch (Exception e) {
                System.out.println("Input file contains invalid inputs!  Exiting...");
                System.exit(0);
            }

            // When the temperature is above 60, add it to the string that lists all the months above 60
            if (temperature > 60)
            {
                //Month.of(i+1) returns a string of the month offset by one since Month.of(1) returns Janurary
                monthsAbove60 += formatMonthAndTemp(temperature, Month.of(i+1).toString());
                numberOfMonthsAbove60 += 1;
            }

            // If the temperature is higher than the highest record temperature so far, reset the temperature string
            // and set a new one
            if (temperature > highestTemperature)
            {
                highestTemperature = temperature;
                highestMonth = Month.of(i+1).toString();
            }
            // Months can have the same average temperature (though unlikely) so if it's the case you print them together
            else if (temperature == highestTemperature)
            {
                highestMonth += ", " + Month.of(i+1).toString();
            }
            totalCount += temperature;
        }

        // Print the average temperature for the year
        System.out.printf("The average temparture for the year is: %.2f \n", totalCount / AnnapolisTemperatures.MONTHSINYEAR);

        // Print the highest temperature of the year
        System.out.printf("The highest temperature of the year was %d during: %s\n", highestTemperature, highestMonth);

        // Conditionals make sure the sentences have the correct grammar and output
        if (numberOfMonthsAbove60 > 1)
        {
            System.out.printf("%d months were above 60 degrees this year:\n%s", numberOfMonthsAbove60, monthsAbove60);
        }
        else if (numberOfMonthsAbove60 == 1)
        {
            System.out.printf("One month was about 60 degrees this year:\n%s", monthsAbove60);
        }
        else
        {
            System.out.println("No months were above 60 degrees this year");
        }

        inputFile.close();
     }
     
     /**
      *   Returns a string formatted in a single spot, so it's easy to change how the output looks
      *
      *     @param temperature the temperature of the month
      *     @param date the month the temperature was at
      *     @return A string formatted like this "July : 80"
      *
      */
     public static String formatMonthAndTemp(int temperature, String date)
     {
         return String.format("%s: %d\n", date, temperature);
     }
 }