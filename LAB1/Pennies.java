/** This program is designed to calculate the total amount of pennies you have given an amount of Dollars, Quarters, Dimes, Nickels,
 *      And Pennies by the user.
 * 
 * @author BJ.Stout
 * 
 **/

import java.util.Scanner;

class Pennies
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);

        String userInput = "";

        int totalPennies = 0;

        // Each section asks for a monetary amount in either Dollars/Quarters/Dimes/Nickels/Pennies and converts that value to pennies
        //  before adding it to the totalPennies amount
        System.out.println("How many dollars do you have?  (Whole dollars only): ");
        userInput = scan.nextLine();
        totalPennies += 100 * ParseInput(userInput);

        System.out.println("How many quarters do you have?: ");
        userInput = scan.nextLine();
        totalPennies += 25 * ParseInput(userInput);

        System.out.println("How many dimes do you have?: ");
        userInput = scan.nextLine();
        totalPennies += 10 * ParseInput(userInput);

        System.out.println("How many nickels do you have?: ");
        userInput = scan.nextLine();
        totalPennies += 5 * ParseInput(userInput);

        System.out.println("How many pennies do you have?: ");
        userInput = scan.nextLine();
        totalPennies += 1 * ParseInput(userInput);

        System.out.println("The total number of pennies you have is: " + totalPennies);

        scan.close();
    }


    // Catch user input that can not be converted to a valid number so program doesn't crash.
    // In the future this can be implemented in Try + Catch loop so it'll run till a proper input is given
    public static int ParseInput(String userInput)
    {
        int retNumber = 0;

        try {
            retNumber = Integer.parseInt(userInput);
        } catch (Exception e) {
            System.out.println("You didn't enter a valid number!  Exiting...");
            System.exit(0);
        }   
        return retNumber;
    }

}