/** This program is designed to calculate the tip, tax, and total bill after eating out at a resturaunt
 *
 * @author BJ.Stout
 * 
 **/
import java.util.Scanner;
import java.lang.Math;

class Restaurant
{

    private static final Double TAX = 0.06;
    private static final Double TIP = 0.15;

    public static void main(String[] args)
    {
            Scanner scan = new Scanner(System.in);

            System.out.println("What is the total charge of the food?: ");
            String userInput = scan.nextLine();

            //Replaces Dollar Bills if user is one of those people.  $10 or 10$ is still expected correct input.
            userInput = userInput.replace("$", "");

            double chargeOfFood = ParseInput(userInput);

            double bill = RoundToSecondDecimal(chargeOfFood);

            // Calculate the bill + the tax. Numbers are rounded to the nearest 100th of a whole number as US Sales Tax is handled this way.
            double chargeOfTax = RoundToSecondDecimal(bill * Restaurant.TAX);
            bill += chargeOfTax;

            // Calculate the bill + the tip.  The Tip includes the total sale (Bill + Tax)
            double chargeOfTip = RoundToSecondDecimal(bill * Restaurant.TIP);
            bill = RoundToSecondDecimal(bill + chargeOfTip);

            // Format the output
            // TODO: Line up all the zeros.
            System.out.println("Cost of food: " + chargeOfFood);
            System.out.println("Cost of tax: " + chargeOfTax);
            System.out.println("Cost of tip: " + chargeOfTip);
            System.out.println("Cost of total bill: " + bill);

            scan.close();
    }

    // Function Rounds a number to the nearest 100th of a whole number.
    //  Math.round normally rounds to the nearest whole number so the decimal point must be moved two places and then back
    //  so none of our desired percision is lost.
    public static double RoundToSecondDecimal(double number)
    {
        return Math.round(number * 100) / 100D;
    }

    // Catch user input that can not be converted to a valid number so program doesn't crash.
    // In the future this can be implemented in Try + Catch loop so it'll run till a proper string is given
    public static double ParseInput(String userInput)
    {
        double returnDouble = 0.0;

        try {
            returnDouble = Double.parseDouble(userInput);
        } catch (Exception e) {
            System.out.println("You didn't enter a valid number!  Exiting...");
            System.exit(0);
        }   
        return returnDouble;
    }
}