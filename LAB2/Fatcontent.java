/**
 * This program determines wether or not a food item is high in fat.
 * 
 * @author Brian J Stout
 */

import java.util.Scanner;

 class Fatcontent
 {
    private static final int CaloriesInFatGram = 9;

     public static void main(String[] args)
     {
        Scanner scan = new Scanner(System.in);
        String userInput = "";
        Double totalCalories = 0D;
        Double fatGrams = 0D;
        Double fatCalories = 0D;

         // Request from the user the total number of calories
        System.out.print("Enter the total number of calories: ");

         userInput = scan.nextLine();
         totalCalories = ParseInputForDouble(userInput);
         if (totalCalories == null || totalCalories < 0)
         {
             System.out.println("Please enter a valid number!  Exiting...");
             System.exit(0);
         } 

         // Request from the user the total number of fat grams
        System.out.print("Enter the number of Fat in Grams: ");

         userInput = scan.nextLine();
         fatGrams = ParseInputForDouble(userInput);
         if (fatGrams == null || totalCalories < 0)
         {
             System.out.println("Please enter a valid number!  Exiting...");
             System.exit(0);
         }

         //Calculate and Print the Results
         PrintResults(totalCalories, fatGrams);

     } 

    /**
     * Given a string it will attempt to convert that string to a number using Double.parseDouble().  If .parseDouble()
     * throws a NumberFormatException it will return a null.
     * 
     * @param userInput the string parsed.
     * @return A converted Double from a string, or null.
     */
    public static Double ParseInputForDouble(String userInput)
    {
        Double returnDouble = 0.0;

        try {
            returnDouble = Double.parseDouble(userInput);
        } catch (Exception e) {
            System.out.println("You didn't enter a valid number!  Exiting...");
            returnDouble = null;
        }   
        return returnDouble;
    }
    
    /**
     * Calculates the ratio of fatcalories to total calories and prints the results, specifying if it's a high in fat food or low in fat food
     * 
     * @param totalCalories the total number of calories in the food option
     * @param fatGrams the total number of fat in grams in the same food
     * 
     */
    public static void PrintResults(Double totalCalories, Double fatGrams)
    {
        Double fatCalories = FatGrams2Calories(fatGrams);
        Double fatRatio = fatCalories / totalCalories;

        // You can not have more than 100% of calories from Fat to Total Calories 
        if (fatRatio > 1)
        {
            System.out.println("Fat calories can not be greater than total calories");
        }
        else if (fatRatio < 0.30f)
        {
            System.out.printf("This is a low fat food option with only %.2f%% fat.", fatRatio * 100);
        }
        else
        {
            System.out.printf("With %.2f%% of it's calories from fat, this is NOT a low fat food option.  Please note:\n", fatRatio * 100);
            System.out.printf("Avocados, nuts, yogurt, eggs, olive, and coconut oils are not low fat, but good for you.\n");
        }
    }

    /**
     * Used to convert grams of fat to calories
     * 
     * @param fatGrams the amount of fat (in grams) being converted
     * @return a double representing the number of calories per gram of fat
     */
    public static Double FatGrams2Calories(Double fatGrams)
    {
        return fatGrams * Fatcontent.CaloriesInFatGram;
    }
 }