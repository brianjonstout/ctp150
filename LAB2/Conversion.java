/**
 * Program converts U.S. Units to Metric Units
 * 
 * @author Brian J Stout
 */

import java.util.Scanner;

 class Conversion
 {

    // Table of Conversions utilized from this source : https://www.isa.org/uploadedFiles/Content/Training_and_Certifications/ISA_Certification/CCST-Conversions-document.pdf
    private static final double YardsInMeter = 1.093613;
    private static final double QuartsInLiter = 1.05669;
    private static final double KelvinCelsiusDifference = 273.15;

    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        String userInput = "";

        System.out.println("Conversion Options:");
        System.out.println("    1. Distance");
        System.out.println("    2. Liquid");
        System.out.println("    3. Temperature");
        System.out.println("    Q. Quit Program");

        Integer option = 0;

        System.out.print("Which conversion would you like to perform? ");

        userInput = scan.nextLine();
        option = ParseInputForMenuOption(userInput);

        // If invalid input, exit out of program
        if(option == null)
        {
            System.out.println("Invalid option!  Please select 1, 2, 3, or Quit with \'q\' or 0");
            System.out.println("Exiting...");
            System.exit(0);
        }

        switch(option)
        {
            // Q, q or 0 exit's the program
            case 0:
                System.out.println("Exiting...");
                System.exit(0);
                break;
            case 1:
                DistanceMenu();
                break;
            case 2:
                LiquidMenu();
                break;
            case 3:
                TemperatureMenu();
                break;
            // Will cover cases where input is valid number, but not a menu option
            default:
                System.out.println("Invalid option!  Please select 1, 2, 3, or Quit with \'q\' or 0");
                System.out.println("Exiting...");
                System.exit(0);

        }
        scan.close();
    }

    /**
     * DistanceMenu() is the function that handles the menu option and decision tree for converting the distance values in the program
     */
    public static void DistanceMenu()
    {
        Scanner scan = new Scanner(System.in);
        String userInput = "";

        System.out.println("Conversion Options: ");
        System.out.println("    1. Meters to Yards");
        System.out.println("    2. Yards to Meters");
        System.out.println("    Q. Quit the Program");

        Integer option = 0;

        System.out.print("Which conversion would you like to perform? ");

        userInput = scan.nextLine();
        option = ParseInputForMenuOption(userInput);

        if(option == null)
        {
            System.out.println("Invalid option!  Please select 1, 2, or Quit with \'q\' or 0");
            System.out.println("Exiting...");
            System.exit(0);
        }

        Double userNumber = 0D;

        switch(option)
        {
            case 0:
                System.out.println("Exiting...");
                System.exit(0);
                break;
            case 1:
                System.out.println("Enter the number of Meters: ");
                userNumber = ParseInputForDouble(scan.nextLine());
                if (userNumber == null)
                {
                    System.out.println("Invalid Input!  Exiting...");
                    System.exit(0);
                    break;
                }
                PrintUnits(userNumber, "Meters", Yards2Meters(userNumber), "Yards");
                break;
            case 2:
                System.out.println("Enter the number of Yards: ");
                userNumber = ParseInputForDouble(scan.nextLine());
                if (userNumber == null)
                {
                    System.out.println("Invalid Input!  Exiting...");
                    System.exit(0);
                    break;
                }
                PrintUnits(userNumber, "Yards", Yards2Meters(userNumber), "Meters");
                break;
            default:
                System.out.println("Invalid option!  Please select 1, 2, or Quit with \'q\' or 0");
                System.out.println("Exiting...");
                System.exit(0);    
        }

        scan.close();
    }

    /**
     * LiquidMenu() is the function that handles the menu option and decision tree for converting the liquid values in the program
     */
    public static void LiquidMenu()
    {
        Scanner scan = new Scanner(System.in);
        String userInput = "";

        System.out.println("Conversion Options: ");
        System.out.println("    1. Liters to Quarts");
        System.out.println("    2. Quarts to Liters");
        System.out.println("    Q. Quit the Program");

        Integer option = 0;

        System.out.print("Which conversion would you like to perform? ");

        userInput = scan.nextLine();
        option = ParseInputForMenuOption(userInput);

        if(option == null)
        {
            System.out.println("Invalid option!  Please select 1, 2, or Quit with \'q\' or 0");
            System.out.println("Exiting...");
            System.exit(0);
        }

        Double userNumber = 0D;

        switch(option)
        {
            case 0:
                System.out.println("Exiting...");
                System.exit(0);
                break;
            case 1:
                System.out.println("Enter the number of Liters: ");
                userNumber = ParseInputForDouble(scan.nextLine());
                if (userNumber == null)
                {
                    System.out.println("Invalid Input!  Exiting...");
                    System.exit(0);
                    break;
                }
                PrintUnits(userNumber, "Liters", Liters2Quarts(userNumber), "Quarts");
                break;
            case 2:
                System.out.println("Enter the number of Quarts: ");
                userNumber = ParseInputForDouble(scan.nextLine());
                if (userNumber == null)
                {
                    System.out.println("Invalid Input!  Exiting...");
                    System.exit(0);
                    break;
                }
                PrintUnits(userNumber, "Quarts", Quarts2Liters(userNumber), "Liters");
                break;
            default:
                System.out.println("Invalid option!  Please select 1, 2, or Quit with \'q\' or 0");
                System.out.println("Exiting...");
                System.exit(0);    
        }
        scan.close();
    }

    /**
     * TemperatureMenu() is the function that handles the menu option and decision tree for converting the temperature values in the program
     */
    public static void TemperatureMenu()
    {
        Scanner scan = new Scanner(System.in);
        String userInput = "";

        System.out.println("Conversion Options: ");
        System.out.println("    1. Kelvin to Celsius");
        System.out.println("    2. Celsius to Kelvin");
        System.out.println("    Q. Quit the Program");

        Integer option = 0;

        System.out.print("Which conversion would you like to perform? ");

        userInput = scan.nextLine();
        option = ParseInputForMenuOption(userInput);

        if(option == null)
        {
            System.out.println("Invalid option!  Please select 1, 2, or Quit with \'q\' or 0");
            System.out.println("Exiting...");
            System.exit(0);
        }

        Double userNumber = 0D;

        switch(option)
        {
            case 0:
                System.out.println("Exiting...");
                System.exit(0);
                break;
            case 1:
                System.out.println("Enter the number of Kelvin: ");
                userNumber = ParseInputForDouble(scan.nextLine());
                if (userNumber == null)
                {
                    System.out.println("Invalid Input!  Exiting...");
                    System.exit(0);
                    break;
                }
                PrintUnits(userNumber, "Kelvin", Kelvin2Celsius(userNumber), "Celsius");
                break;
            case 2:
                System.out.println("Enter the number of Celsius: ");
                userNumber = ParseInputForDouble(scan.nextLine());
                if (userNumber == null)
                {
                    System.out.println("Invalid Input!  Exiting...");
                    System.exit(0);
                    break;
                }
                PrintUnits(userNumber, "Celsius", Celsius2Kelvin(userNumber), "Kelvin");
                break;
            default:
                System.out.println("Invalid option!  Please select 1, 2, or Quit with \'q\' or 0");
                System.out.println("Exiting...");
                System.exit(0);    
        }
        scan.close();
    }

    /**
     * Returns a number in Meters equivalent to the inputed value of Yards
     * 
     * @param units the amount of Yards being converted to Meters.
     * @return an equivalent distance value in Meters.
     */
    public static double Yards2Meters(double units)
    {
        return units / Conversion.YardsInMeter;
    }

    /**
     * Returns a number in Yards equiavlent to the inputed value of Meters
     * 
     * @param units the amount of Meters being converted to Yards.
     * @return an equivalent distance value in Yards.
     */
    public static double Meters2Yards(double units)
    {
        return units * Conversion.YardsInMeter;
    }

    /**
     * Returns a number in Liters equivalent to the inputed value of Quarts
     * 
     * @param units the amount of Quarts being converted to Liters.
     * @return an equivalent liquid volume value in Liters.
     */
    public static double Quarts2Liters(double units)
    {
        return units * Conversion.QuartsInLiter;
    }

    /**
     * Returns a number in Quarts equivalent to the inputed value in Liters
     * 
     * @param units the amount of Liters being converted to Quarts.
     * @return an equivalent liquid volume value in Quarts.
     */
    public static double Liters2Quarts(double units)
    {
        return units / Conversion.QuartsInLiter;
    }

    /**
     * Returns a number in Kelvin equivalent to the inputed value in Celsius
     * 
     * @param units the amount of Celsius being converted to Kelvin.
     * @return an equivalent temperature value in Kelvin
     */
    public static double Celsius2Kelvin(double units)
    {
        return units + Conversion.KelvinCelsiusDifference;
    }

    /**
     * Returns a number in Celsius equivalent to the inputed value in Kelvin
     * 
     * @param units the amount of Kelvin being converted to Celsius.
     * @return an equivalent temperature value in Celsius.
     */
    public static double Kelvin2Celsius(double units)
    {
        return units - Conversion.KelvinCelsiusDifference;
    }

    /**
     * Given a string it will attempt to convert that string to a number using Double.parseDouble().  If .parseDouble()
     * throws a NumberFormatException it will return a null.
     * 
     * @param userInput the string parsed.
     * @return A converted Double from a string, or null.
     */
    public static Double ParseInputForDouble(String userInput)
    {
        Double returnDouble = 0.0;

        try {
            returnDouble = Double.parseDouble(userInput);
        } catch (Exception e) {
            System.out.println("You didn't enter a valid number!  Exiting...");
            returnDouble = null;
        }   
        return returnDouble;
    }

    /**
     * Given a string it will atempt to convert that string to an umber using Integer.parseInt().  If .parseInt()
     * throws a number FormatException, it will return a null.  If the useInput is 'q'/'Q' it will return a 0.
     * 
     * @param userInput the string parsed.
     * @return a converted Int from a string, or null.
     */
    public static Integer ParseInputForMenuOption(String userInput)
    {
        Integer returnInt = 0;

        try {
            if (Character.toLowerCase(userInput.charAt(0)) == 'q' && userInput.length() == 1)
            {
                returnInt = 0;
            }
            else
            {
                returnInt = Integer.parseInt(userInput);
            }

        } catch (Exception e) {
            returnInt = null;
        }
        return returnInt;
    }

    /**
     * Will print out a formatted string using a 2 decimal percision for any provided floats or doubles
     * 
     * @param number1 the first number being printed.
     * @param unit1 the unit of measurement for the first value.
     * @param number2 the second number being printed.
     * @param unit2 the unit of measurement for the second value.
     */
    public static void PrintUnits(Double number1, String unit1, Double number2, String unit2)
    {
        System.out.printf("%.2f %s converts to %.2f %s", number1, unit1, number2, unit2);
    }
 }