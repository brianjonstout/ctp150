/**
 * @author Brian J Stout
 */
import java.util.Objects;

class FacultyCollegeEmployee extends CollegeEmployee
{
    private Double yearlyPay = 0D;
    private String department = "";
    private static final Double weeksInSalaryYear = 52D;

    /**
     * Default constructor
     */
    public FacultyCollegeEmployee()
    {
        super();
        this.yearlyPay = 0D;
        this.department = "";
    }

    /**
     * Parameter constructor
     * @param name name of the Employee
     * @param yearlyPay the yearly salary of the Employee
     * @param department the department the Employee works in
     */
    public FacultyCollegeEmployee(String name, Double yearlyPay, String department)
    {
        super(name);
        this.yearlyPay = yearlyPay;
        this.department = department;
    }

    /**
     * Copy constructor
     * @param obj
     */
    public FacultyCollegeEmployee(FacultyCollegeEmployee obj)
    {
        this(obj.getName(), obj.getYearlyPay(), obj.getDepartment());
    }

    /**
     * Returns the weekly salary of the employee
     * @return the yearly pay divided by the weeks in a year
     */
    public Double getSalary()
    {
        return this.yearlyPay / FacultyCollegeEmployee.weeksInSalaryYear;
    }

    /**
     * Getter for the yearly salary field
     * @return
     */
    public Double getYearlyPay()
    {
        return this.yearlyPay;
    }

    /**
     * Setter for the yearly salary field
     * @param yearlyPay
     */
    public void setYearlyPay(Double yearlyPay)
    {
        this.yearlyPay = yearlyPay;
    }

    /**
     * Getter for the department field
     * @return
     */
    public String getDepartment()
    {
        return this.department;
    }

    /**
     * Setter for the department field
     * @param department
     */
    public void setDepartment(String department)
    {
        this.department = department;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == this)
        {
            return true;
        }
        if ((obj instanceof FacultyCollegeEmployee) == false)
        {
            return false;
        }

        FacultyCollegeEmployee facultyCollegeEmployee = (FacultyCollegeEmployee)obj;
        return Objects.equals(this.yearlyPay, facultyCollegeEmployee.yearlyPay) && Objects.equals(this.department, facultyCollegeEmployee.department) && super.equals(obj);
    }

    @Override
    public String toString()
    {
        return super.toString() +
                String.format("    %15s%15.2f", "Yearly Pay:", this.getYearlyPay()) +
                String.format("    %15s%15s", "Department:", this.getDepartment());
    }
}