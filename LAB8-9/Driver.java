/**
 * Driver is a program that demonstrates the use of the CollegeEmployee super class and its sub classes.
 * @author Brian J Stout
 */

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Collections;
import java.io.*;

class Driver
{
    static private final int CELLCOUNT = 4;
    static private final String filePath = "employees-1.csv";
    static private final String menu =  "1. Print all Employees\n" +
                                        "2. Search Employees by Last Name\n" +
                                        "3. Search Employees by Department\n" +
                                        "4. Search for Employees who make greater than inputted Salary\n" +
                                        "5. Search for Employees by type (Faculty, Sales, or Staff)\n" +
                                        "6. Exit the program\n";
    public static void main(String[] args) throws FileNotFoundException
    {
        ArrayList<CollegeEmployee> list = returnCollegeEmployeeArrayList(Driver.filePath);
        mainMenu(list);
    }

    /**
     * The main loop of the driver, has the option to select all sub-menus and exit
     * @param list A list of college employees
     */
    static public void mainMenu(ArrayList<CollegeEmployee> list)
    {
        Scanner IOScan = new Scanner(System.in);
        ArrayList<CollegeEmployee> results = null;

        while(true)
        {
            System.out.print(Driver.menu);
            System.out.print("Enter Choice: ");
            String input = "";
            input = IOScan.nextLine();
            System.out.println();

            // Inputted string should only be one character
            if(input.length() != 1)
            {
                input = "0";
            }
    
            switch(input.charAt(0))
            {
                case '1':
                    printCollegeEmployeeArraylist(list);
                    System.out.println();
                    break;

                case '2':
                    System.out.print("Enter a last name: ");
                    input = IOScan.nextLine();
                    results = searchCollegeEmployeeListByName(list, input);

                    if(results.isEmpty() == false)
                    {
                        System.out.println("\nResults:");
                        printCollegeEmployeeArraylist(results);
                        System.out.println();
                    }
                    else
                    {
                        System.out.println("No Results found\n");
                    }
                    break;

                case '3':
                    System.out.print("Enter a Department: ");
                    input = IOScan.nextLine();
                    results = searchFacultyByDepartment(list, input);

                    if(results.isEmpty() == false)
                    {
                        System.out.println("\nResults:");
                        printCollegeEmployeeArraylist(results);
                        System.out.println();
                    }
                    else
                    {
                        System.out.println("No Results found\n");
                    }
                    break;

                case '4':
                    System.out.print("Enter a Salary: ");
                    input = IOScan.nextLine();
                    results = searchForEmployeesBySalary(list, parseInputForDouble(input));

                    if(results.isEmpty() == false)
                    {
                        System.out.println("\nResults:");
                        printCollegeEmployeeArraylist(results);
                        System.out.println();
                    }
                    else
                    {
                        System.out.println("No Results found\n");
                    }
                    break;

                case '5':
                    typeSelectionMenu(list);
                    System.out.println();
                    break;
                case '6':
                    System.out.println("Exitting...");
                    IOScan.close();
                    System.exit(0);
                    break;
                default:
                    System.out.println("Not a valid option\n");
                    continue;
            }
        }
    }


    /**
     * Secondary looop of the driver, has the option to print employees by their employment type
     * @param list A list of college employees
     */
    public static void typeSelectionMenu(ArrayList<CollegeEmployee> list)
    {
        Scanner IOScan = new Scanner(System.in);
        ArrayList<CollegeEmployee> results = null;

        String menu =   "1. Faculty College Employees\n" +
        "2. Staff College Employees\n" +
        "3. Sales Recruiter College Employees\n" +
        "4. Exit\n";

        while(true)
        {

            System.out.print(menu);
            System.out.print("Enter Choice: ");
            String input = "";
            input = IOScan.nextLine();
            System.out.println();

            // Inputted string should only be one character
            if(input.length() != 1)
            {
                input = "0";
            }
    
            switch(input.charAt(0))
            {
                case '1':
                    for(CollegeEmployee i : list)
                    {
                        // Print out if employee is faculty
                        if(i instanceof FacultyCollegeEmployee)
                        {
                            System.out.println(i);
                        }
                    }
                    return;

                case '2':
                    for(CollegeEmployee i : list)
                    {
                        // Print out if employee is staff
                        if(i instanceof StaffCollegeEmployee)
                        {
                            System.out.println(i);
                        }
                    }
                    return;

                case '3':
                    for(CollegeEmployee i : list)
                    {
                        // Print out if employee is in sales
                        if(i instanceof SalesRecruiterCollegeEmployee)
                        {
                            System.out.println(i);
                        }
                    }
                    return;

                case '4':
                    System.out.println("Exiting Menu...\n");
                    return;
                default:
                    System.out.println("Not a valid option\n");
                    continue;
            }
        }
    }


    /**
     * Given a filepath the csv file containing employees will be searched for, and if found parsed with generated employees being put into an array list
     * @param filePath the file path to the employee csv file
     * @return an array list containing Employees
     * @throws FileNotFoundException Program will exit if the file does not exit at the path, but sould not throw a FileNotFoundException
     */
    public static ArrayList<CollegeEmployee> returnCollegeEmployeeArrayList(String filePath) throws FileNotFoundException
    {
        ArrayList<CollegeEmployee> list = createEmptyCollegeEmployeeArrayList();

        Scanner fileScan = openData(filePath);
        while(fileScan.hasNext())
        {
            CollegeEmployee newEmployee = readCollegeEmployee(fileScan);
            if (newEmployee != null)
            {
                list.add(newEmployee);
            }
        }

        closeData(fileScan);
        return list;
    }

    /**
     * Given a filepath openData() will check to see if a file exists at the location, and then open it up returning a Scanner to that object.
     *  If the file does not exit the program will Error and exit
     * @param filePath the filepath to the file
     * @return a scanner to the file
     * @throws FileNotFoundException Program will exit if the file does not exit at the path, but sould not throw a FileNotFoundException
     */
    public static Scanner openData(String filePath) throws FileNotFoundException
    {
        File file = new File(filePath);

        if(!file.exists() || !file.isFile())
        {
            System.out.println("File: " + file + " does not exist, or is not a file");
            System.exit(0);
        }

        return new Scanner(file);
    }


    /**
     * Will read a single record from a Scanner object pointed to the employee csv
     * @param fileScan the scanner pointing to an employee CSV
     * @return a single CollegeEmployee object
     */
    public static CollegeEmployee readCollegeEmployee(Scanner fileScan)
    {
        if(fileScan.hasNext() == false)
        {
            System.out.println("ERROR: readData() called on empty file");
            return null;     
        }

        String record = fileScan.nextLine();
        String[] cell = record.split(",");

        //String.split won't add empty data so ,,, will return a length of 0
        if(cell.length == 0)
        {
            return null;
        }

        // Indicates a improperly formatted record (missing value most likely)
        // TODO:  Not required currently, but cell count should be implemented per type of record, and not for the entire file
        if(cell.length != CELLCOUNT)
        {
            System.out.println("Cellcount " + cell.length);
            System.out.println( "Error: input file contains improperly formatted cells.\n Row: \"" + record +
                                "\" should contain 4 items separated by 3 commas.  Exiting...");
            System.exit(0);
        }

        switch(cell[0].charAt(0))
        {
            case 's':
                return new StaffCollegeEmployee(cell[1], parseInputForDouble(cell[2]), parseInputForDouble(cell[3]));
            case 'r':
                return new SalesRecruiterCollegeEmployee(cell[1], parseInputForDouble(cell[2]), cell[3].charAt(0));
            case 'f':
                return new FacultyCollegeEmployee(cell[1], parseInputForDouble(cell[2]), cell[3]);
            default:
                System.out.println("Error:  Input file contained an improperly formatted record, \"" + cell[0] + "\" is not a valid option, it should be s,r, or f");
                return null;
        }
    }

    /**
     * Closes the scanner
     * @param fileScan scanner to be closed
     */
    public static void closeData(Scanner fileScan)
    {
        fileScan.close();
    }

    /**
     * Returns a empty Arraylist object containing CollegeEmployee objects.
     * @return An empty ArrayList containing CollegeEmployee objects
     */
    public static ArrayList<CollegeEmployee> createEmptyCollegeEmployeeArrayList()
    {
        return new ArrayList<CollegeEmployee>();
    }

    /**
     * Prints out an ArrayList of College Employees
     * @param list the list to be printed out
     */
    public static void printCollegeEmployeeArraylist(ArrayList<CollegeEmployee> list)
    {
        for(CollegeEmployee i : list)
        {
            System.out.println(i);
        }
    }

    /**
     * Steps through an array list of college employees, returning a new array list containing employees who have the same
     *  last name as @param name
     * @param list the list being searched through
     * @param name the name being matched to the employees' last name
     * @return a new list containing matches
     */
    public static ArrayList<CollegeEmployee> searchCollegeEmployeeListByName(ArrayList<CollegeEmployee> list, String name)
    {

        ArrayList<CollegeEmployee> returnList = createEmptyCollegeEmployeeArrayList();

        for (CollegeEmployee i : list)
        {
            if (i != null)
            {
                String[] employeeName = i.getName().split(" ");

                // Checks to see if the employee did not have an empty name
                if(employeeName != null && employeeName.length > 0)
                {
                    String lastName = employeeName[employeeName.length - 1];  //Grabs the last name, or only name if a full name wasn't inputted
                    if(name.equalsIgnoreCase(lastName))
                    {
                        returnList.add(i);
                    }
                }
            }
        }

        return returnList;
    }

    public static ArrayList<CollegeEmployee> searchFacultyByDepartment(ArrayList<CollegeEmployee> list, String department)
    {
        ArrayList<CollegeEmployee> returnList = createEmptyCollegeEmployeeArrayList();

        for(CollegeEmployee i : list)
        {
            if((i instanceof FacultyCollegeEmployee) == true)
            {
                FacultyCollegeEmployee employee = (FacultyCollegeEmployee)i;
                if(department.equalsIgnoreCase(employee.getDepartment()))
                {
                    returnList.add(i);
                }
            }
        }

        return returnList;
    }

    /**
     * Steps through an array list of college employees, returning a new array list containg employees who have a weekly Salary
     *  higher than the salary given by @param salary
     * @param list the list being searched through
     * @param salary the salary being matched
     * @return a list containing matches
     */
    public static ArrayList<CollegeEmployee> searchForEmployeesBySalary(ArrayList<CollegeEmployee> list, Double salary)
    {
        ArrayList<CollegeEmployee> returnList = createEmptyCollegeEmployeeArrayList();

        for(CollegeEmployee i : list)
        {
            if(i != null)
            {
                if(i.getSalary() > salary)
                {
                    returnList.add(i);
                }
            }
        }
        return returnList;
    }

    /**
     * Given a string it will attempt to convert that string to a number using Double.parseDouble().  If .parseDouble()
     * throws a NumberFormatException it will return a null.
     * 
     * @param userInput the string parsed.
     * @return A converted Double from a string, or null.
     */
    public static Double parseInputForDouble(String userInput)
    {
        Double returnDouble = 0.0;

        try {
            returnDouble = Double.parseDouble(userInput);
        } catch (Exception e) {
            System.out.println( "Error: input file constains improperly formatted cells.\nprice: \"" + userInput +
                                "\" could not be converted to a number.  Exiting...");
            System.exit(0);
        }   
        return returnDouble;
    }
}