/**
 * @author Brian J Stout
 */

import java.util.Objects;

class SalesRecruiterCollegeEmployee extends CollegeEmployee
{
    private Double salesAmount = 0D;
    private Character type = null;
    private static final Double commission = 0.10;

    /**
     * Converts the type field into a string representation
     * @return o = Office, h = Home
     */
    public String getTypeString()
    {
        switch(this.getType())
        {
            case 'o':
                return "Office";
            case 'h':
                return "Home";
            // Indicates unknown or corrupt data
            default:
                return "Unknown ("+ this.getType() + ")";
        }
    }

    /**
     * Default constructor
     */
    public SalesRecruiterCollegeEmployee()
    {
        super();
        this.salesAmount = 0D;
        this.type = null;
    }

    /**
     * Parameter constructor
     * @param name the name of the employee
     * @param salesAmount the amount of sales the employee has in Dollars
     * @param type where the employee works from
     */
    public SalesRecruiterCollegeEmployee(String name, Double salesAmount, Character type)
    {
        super(name);
        this.salesAmount = salesAmount;
        this.type = type;
    }

    /**
     * Copy Constructor
     * @param obj
     */
    public SalesRecruiterCollegeEmployee(SalesRecruiterCollegeEmployee obj)
    {
        this(obj.getName(), obj.getSalesAmount(), obj.getType());
    }

    /**
     * Calculates the salary of the employee
     * @return the salary based on the employee's commission times his sales
     */
    public Double getSalary()
    {
        return salesAmount * SalesRecruiterCollegeEmployee.commission;
    }

    /**
     * Getter for the salesAmount field
     * @return
     */
    public Double getSalesAmount()
    {
        return this.salesAmount;
    }

    /**
     * Setter for the salesAmount field
     * @param salesAmount
     */
    public void setSalesAmount(Double salesAmount)
    {
        this.salesAmount = salesAmount;
    }

    /**
     * Getter for the type field
     * @return
     */
    public Character getType()
    {
        return this.type;
    }

    /**
     * Setter for the type field
     * @param type
     */
    public void setType(Character type)
    {
        this.type = type;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == this)
        {
            return true;
        }

        if ((obj instanceof SalesRecruiterCollegeEmployee) == false)
        {
            return false;
        }

        SalesRecruiterCollegeEmployee salesRecruiterCollegeEmployee = (SalesRecruiterCollegeEmployee)obj;
        return Objects.equals(this.salesAmount, salesRecruiterCollegeEmployee.salesAmount) && Objects.equals(this.type, salesRecruiterCollegeEmployee.type) && super.equals(obj);
    }

    @Override
    public String toString()
    {
        return super.toString() +
                String.format("    %15s%15.2f", "Sales:", this.getSalesAmount()) +
                String.format("    %15s%15s", "Type:", this.getTypeString());
    }
}