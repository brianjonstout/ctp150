/**
 * @author Brian Jon Stout
 */

import java.util.Objects;

abstract class CollegeEmployee
{
    private String name = "";

    /**
     * Default constructor
     */
    public CollegeEmployee()
    {
        this.name = "";
    }

    /**
     * Parameter constructor
     * @param name
     */
    public CollegeEmployee(String name)
    {
        this.name = name;
    }

    /**
     * Copy constructor
     * @param obj
     */
    public CollegeEmployee(CollegeEmployee obj)
    {
        this.name = obj.getName();
    }

    /**
     * Getter for name field
     * @return
     */
    public String getName()
    {
        return this.name;
    }

    /**
     * Setter for name field
     * @param name
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Returns salary of employee, implemented by sub classes
     * @return
     */
    abstract public Double getSalary();

    @Override
    public boolean equals(Object obj) {

        if (obj == this)
        {
            return true;
        }

        if ((obj instanceof CollegeEmployee) == false)
        {
            return false;
        }

        CollegeEmployee collegeEmployee = (CollegeEmployee)obj;
        return Objects.equals(this.name, collegeEmployee.name);
    }

    @Override
    public String toString()
    {
        return String.format("%s%15s    %s$%15.2f", "Name:", this.getName(), "Salary:", this.getSalary());
    }
}