/**
 * @author Brian J Stout
 */

import java.util.Objects;

class StaffCollegeEmployee extends CollegeEmployee
{
    private Double hoursWorked = 0D;
    private Double payRate = 0D;
    private static final Double overtimeRate = 1.5;
    private static final Double overtimeHours = 40D;

    /**
     * Default constructor
     */
    public StaffCollegeEmployee()
    {
        this.hoursWorked = 0D;
        this.payRate = 0D;
    }

    /**
     * Parameter constructor
     * @param name the name of the employee
     * @param hoursWorked the hours worked this week by the employee
     * @param payRate the employee's hourly rate
     */
    public StaffCollegeEmployee(String name, Double hoursWorked, Double payRate)
    {
        super(name);
        this.hoursWorked = hoursWorked;
        this.payRate = payRate;
    }

    /**
     * Copy constructor
     * @param obj
     */
    public StaffCollegeEmployee(StaffCollegeEmployee obj)
    {
        this(obj.getName(), obj.getHoursWorked(), obj.getPayRate());
    }

    /**
     * Calculates the weekly salary of the employee
     * @return the weekly salary by hours worked times their payrate, in addition to overtime hours
     */
    public Double getSalary()
    {
        Double overtime = 0D;

        if (this.hoursWorked > StaffCollegeEmployee.overtimeHours)
        {
            overtime = hoursWorked - StaffCollegeEmployee.overtimeHours;
        }

        return (this.hoursWorked * this.payRate) + (overtime * (this.payRate * StaffCollegeEmployee.overtimeRate));
    }

    /**
     * Getter for hoursWorked field
     * @return
     */
    public Double getHoursWorked()
    {
        return this.hoursWorked;
    }

    /**
     * Setter for hoursWorked field
     * @param hoursWorked
     */
    public void setHoursWorked(Double hoursWorked) {
        this.hoursWorked = hoursWorked;
    }

    /**
     * Getter for payRate field
     * @return
     */
    public Double getPayRate() {
        return this.payRate;
    }

    /**
     * Setter for payRate field
     * @param payRate
     */
    public void setPayRate(Double payRate) {
        this.payRate = payRate;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
        {
            return true;
        }

        if ((obj instanceof StaffCollegeEmployee) == false)
        {
            return false;
        }

        StaffCollegeEmployee staffCollegeEmployee = (StaffCollegeEmployee)obj;
        return Objects.equals(this.hoursWorked, staffCollegeEmployee.hoursWorked) && Objects.equals(this.payRate, staffCollegeEmployee.payRate) && super.equals(obj);
    }

    @Override
    public String toString()
    {
        return super.toString() +
                String.format("    %15s%15.2f", "Hours worked:", this.hoursWorked) +
                String.format("    %15s%15.2f", "Pay Rate:", this.payRate);
    }
}