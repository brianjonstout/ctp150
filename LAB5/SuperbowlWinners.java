/** SupebowlWinners class is a class which takes a list of superbowl winners, one winner by line, and constructs
 *      an array for them.  It also provides methods to look up and search through the array in the context of Football
 * 
 *  The class assumes that each line is valid superbowl Winner starting at 2019 all the way to 1967 when GreenBay first won
 * 
 * @author Brian J Stout
 */
import java.util.Scanner;
import java.io.*;

class SuperbowlWinners
{
    public static final int LEAGUESTARTDATE = 1967;
    public static final int LEAGUELASTSUPERBOWLDATE = 2019;  //TODO:  This can be dynamically found
    private static final String RECORDS = "Superbowl Winners";  // Name of the file provided in class, change this for different file inputs
    private String[] winnerArray = null;

    /** Default constructor for the file, it won't create an instance unless RECORDS is a valid file
     * 
     * @throws FileNotFoundException Thrown if RECORDS is not a valid file path however program should exit before this
     */
    public SuperbowlWinners() throws FileNotFoundException
    {
        File file = new File(RECORDS);

        if(!file.exists() || !file.isFile())
        {
            System.out.println("File: " + file + " does not exist, or is not a file");
            System.exit(0);
        }

        // Create a new string array that will contain enough memory for whatever file provided
        int numberOfRecords = countLinesInFile(file);
        winnerArray = new String[numberOfRecords];

        Scanner inputFile = new Scanner(file);

        //Go through each index and assign it line by line
        for(int i = 0; inputFile.hasNext(); i++)
        {
            winnerArray[i] = inputFile.nextLine();
        }

        inputFile.close();

    }

    /** Using a provided file from the constructor make an instance of SuperbowlWinners.  This is so
     *      a developer can do their own file path/checking instead of using the default file name
     * 
     * @param file
     * @throws FileNotFoundException Thrown if file is not a valid file path however program should exit before this
     */
    public SuperbowlWinners(File file) throws FileNotFoundException
    {
        if(!file.exists() || !file.isFile())
        {
            System.out.println("File: " + file + " does not exist, or is not a file");
            System.exit(0);
        }
        int numberOfRecords = countLinesInFile(file);

        winnerArray = new String[numberOfRecords];
        Scanner inputFile = new Scanner(file);

        for(int i = 0; inputFile.hasNext(); i++)
        {
            winnerArray[i] = inputFile.nextLine();
        }
    }

    /** Constructor for SuperbowlWinners using a prebuilt array.  This is so a developer can provide their own array
     *      from a parsed file such as if Superowl Winners was converted to CSV
     * 
     *  A developer can also initiate an empty SuperbowlWinners instance using an empty string array;
     * 
     * @param array A string array containing a list of superbowl winners
     */
    public SuperbowlWinners(String[] array)
    {
        winnerArray = array;
    }

    /** Prints out the list of winners, and the year they won in Descending order, or from most Recent to least Recent
     * 
     */
    public void showWinners()
    {
        for(int i = 0; i < winnerArray.length; i++)
        {
            System.out.println(winnerArray[i] + ": " + returnYearFromIndex(i));
        }

        System.out.println();
    }

    /** Prints out the list of winners, and the year they won in Ascending order, or from least recent, to most recent
     * 
     */
    public void showWinnersByYear()
    {
        for(int i = winnerArray.length; i > 0; i--)
        {
            System.out.println(winnerArray[i-1] + ": " + returnYearFromIndex(i-1));
        }

        System.out.println();
    }

    /** Prints out the number of times a club has won the super bowl
     * 
     * @param club the club being searched for
     */
    public void findWinner(String club)
    {
        int winAmount = 0;


        for (int i = 0; i < winnerArray.length; i++)
        {
            String winnerCmp = winnerArray[i].toLowerCase();

            if(winnerCmp.contains(club.toLowerCase()))
            {
                winAmount++;
            }
        }

        if(winAmount > 0)
        {
            String time = " time";
            if(winAmount > 1) time += 's';
            System.out.println(club + " has won the Superbowl " + winAmount + time);
        }
        else
        {
            System.out.println(club + " is not on a list of Superbowl Winners");
        }

        System.out.println();
    }

    /** Prints out the number of years that a club has won the superbowl
     * 
     * @param club the club being searched for
     */
    public void findYears(String club)
    {
        Boolean winFlag = false;

        for (int i = 0; i < winnerArray.length; i++)
        {
            String winnerCmp = winnerArray[i].toLowerCase();

            if(winnerCmp.contains(club.toLowerCase()))
            {
                winFlag = true;
                System.out.println(returnYearFromIndex(i));
            }
        }

        if(!winFlag)
        {
            System.out.println(club + " is not on a list of Superbowl Winners");
        }
        System.out.println();
    }

    /** Prints out the winner of the superbowl in a given year
     * 
     * @param year the year being searched
     */
    public void winner(int year)
    {
        System.out.println("Your " + year + " Super bowl winners is " + winnerArray[returnIndexFromYear(year)]);
    }

    /** Parses through a file to see how many lines it contains ignoring empty newlines
     * 
     * @param file the file being parsed
     * @return an integer representing the number strings seperated by newlines in a file
     * @throws FileNotFoundException If the provided file is not a valid file to parse (Does not exist, or is a directory)
     */
    private int countLinesInFile(File file) throws FileNotFoundException
    {
        Scanner inputFile = new Scanner(file);
        int lineCount = 0;

        while(inputFile.hasNext())
        {
            String input = inputFile.nextLine();
            if(inputFile.equals("\n"))
            {
                continue;
            }
            lineCount++;
        }

        inputFile.close();

        return lineCount;
    }

    /** Returns the full year corresponding to the Arrays index.  Since index 0 is the more recent year, 0 will return the date
     *      of the most recent superbowl, winnerArray[winnerArray.length-1] will return 1967 being the first year the Superbowl was held
     * 
     * @param i the index the year is wanted from
     * @return the year corresponding to the index
     */
    private static int returnYearFromIndex(int i)
    {
        return LEAGUELASTSUPERBOWLDATE - i;
    }

    /** Returns the index corresponding to the year.  Like returnYearFromIndex the most recent superbowl year will return 0 whereas 1967 will return winnerArray.length-1
     * 
     * @param year  the year the index is wanted from
     * @return the index corresponding to the year
     */
    private static int returnIndexFromYear(int year)
    {
        return LEAGUELASTSUPERBOWLDATE - year;
    }
}