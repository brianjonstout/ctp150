/** The driver for the SuperbowlWinners class.  It's mainly input validation and menu's
 *    For option 3 and 4 keep in mind that there's no context for names. Example: In option 3 "New England" will report
 *    winning 6 times but "Patriots" will only report 3 times because of name changes and how the input file was formatted
 * 
 * @author Brian J Stout
 */

import java.util.Scanner;
import java.io.*;

class Client
{
    public static Scanner scan = null; //So we can access IO from one place

    public static final String menu =   "1 - Show all winners - descending years\n" +
                                        "2 - Show all winners - ascending years\n" +
                                        "3 - Show how many times a particular team has won\n" +
                                        "4 - Show the years that a particular team has won\n" +
                                        "5 - Show the winner for a particular year\n" +
                                        "6 - Exit\n";
    public static void main (String[] args) throws FileNotFoundException
    {

        SuperbowlWinners winners = new SuperbowlWinners();

        scan = new Scanner(System.in);
        String input = "";

        while(true)
        {
            System.out.print(menu);
            System.out.print("Enter Choice: ");
            input = scan.nextLine();

            // Inputted string should only be one character
            if(input.length() != 1)
            {
                input = "0";
            }
    
            switch(input.charAt(0))
            {
                case '1':
                    winnersDescending(winners);
                    break;
                case '2':
                    winnersAscending(winners);
                    break;
                case '3':
                    teamByWinCount(winners);
                    break;
                case '4':
                    yearsWonByTeam(winners);
                    break;
                case '5':
                    championForYear(winners);
                    break;
                case '6':
                    scan.close();
                    System.exit(0);
                    break;
                default:
                    System.out.println("Not a valid option\n");
                    continue;
            }
        }
    }


    /**  Show all winners - descending years
     * 
     *  @param winners the object which contains the superbowl data
     */
    public static void winnersDescending(SuperbowlWinners winners)
    {   
        winners.showWinners();
    }


    /** Show all winners - ascending years
     * 
     * @param winners the object which contains the superbowl data
     */
    public static void winnersAscending(SuperbowlWinners winners)
    {
        winners.showWinnersByYear();
    }


    /** Show how many times a particular team has won
     * 
     * @param winners the object which contains the superbowl data
     */
    public static void teamByWinCount(SuperbowlWinners winners)
    {
        System.out.print("Enter team name: ");
        String input = "";
        input = scan.nextLine();

        winners.findWinner(input);
    }


    /** Show the years that a particular team has won
     * 
     * @param winners the object which contains the superbowl data
     */
    public static void yearsWonByTeam(SuperbowlWinners winners)
    {
        System.out.print("Enter team name: ");
        String input = "";
        input = scan.nextLine();

        winners.findYears(input);
    }

    /** Show the winner for a particular year.  Also validates input for the years (So a user can't search for a year where a superbowl has not occured)
     * 
     * @param winners the object which contains the superbowl data
     */
    public static void championForYear(SuperbowlWinners winners)
    {
        System.out.print("Enter a year: ");
        String input = "";
        input = scan.nextLine();
        Integer year = ParseInput(input);

        if (year == null)
        {
            return;
        }

        if (year < SuperbowlWinners.LEAGUESTARTDATE || year > SuperbowlWinners.LEAGUELASTSUPERBOWLDATE)
        {
            System.out.println("Please enter a valid date from 1967 to 2019\n");
            return;
        }

        winners.winner(year);
        System.out.println();

    }

    /**  Attempts to convert a string into an Integer, if it can't invalid input is reported
     * 
     * @param userInput the string being converted
     * @return the Integer equivalent of the string provided, or if an exception occurs, null
     */
    public static Integer ParseInput(String userInput)
    {
        Integer retNumber = 0;

        try {
            retNumber = Integer.parseInt(userInput);
        } catch (Exception e) {
            System.out.println("You didn't enter a valid number!\n");
            retNumber = null;
        }
        return retNumber;
    }
}