/**
 * ExamScores class which takes an Array of doubles and provides methods for printing out averages, and validating test grades
 * 
 * @author Brian J Stout
 */

import java.util.Arrays;

class ExamScores
{
    private double[] testScores = null;

    /**
     * Default constructor
     */
    public ExamScores()
    {
        this.testScores = null;
    }

    /**
     * Constructor which takes an array
     * 
     * @param testScores the array of test scores
     * @throws IllegalArgumentException if a value in the array is less than 0 or greater than 100 IllegalArgument will be thrown
     */
    public ExamScores(double[] testScores) throws IllegalArgumentException
    {
        this.testScores = testScores;
        checkArrayIntegrity();
    }

    /**
     * Calculates the average
     * @return the average of the array
     */
    public double getAverage()
    {
        double total = 0;

        for (double i : getTestScores())
        {
            total += i;
        }

        return total / getTestScores().length;
    }

    /**
     * Getter for the testScores array.  Should not be used to directly modify the array.
     * If it is, run checkArrayIntegrity() afterwards.
     * @return the testScores array
     */
    public double[] getTestScores()
    {
        return this.testScores;
    }

    /**
     * Setter for the test scores array.  Array validity will be rechecked.
     * 
     * @param testScores the replacement array
     * @throws IllegalArgumentException 
     */
    public void setTestScores(double[] testScores) throws IllegalArgumentException
    {
        this.testScores = testScores;
        checkArrayIntegrity();
    }

    /**
     * Goes through each value in the array and makes sure it is not less than 0 or greater than 100
     * 
     * @throws IllegalArgumentException if the array fails the check, IllegalArgumentException will be thrown
     */
    public void checkArrayIntegrity() throws IllegalArgumentException
    {
        for(double i : getTestScores())
        {
            if (i < 0)
            {
                throw new IllegalArgumentException("Test scores cannot be less than 0");
            }
            if(i > 100)
            {
                throw new IllegalArgumentException("Test scores cannot be greater than 100");
            }
        }
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == this)
        {
            return true;
        }

        if (!(obj instanceof ExamScores))
        {
            return false;
        }

        ExamScores examScores = (ExamScores) obj;
        return Arrays.equals(examScores.getTestScores(), this.getTestScores());
    }

    @Override
    public String toString()
    {
        return String.format("%s %d\t%s %.2f","Number of Grades:", this.getTestScores().length, "Average:", this.getAverage());
    }
}