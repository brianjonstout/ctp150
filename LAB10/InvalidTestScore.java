/**
 * Custom checked extension which is used to indicate an invalid test score (Less than 0, Greater than 100)
 * 
 * @author Brian J Stout
 */

class InvalidTestScore extends Exception
{

    /**
     * Default constructor
     */
    public InvalidTestScore()
    {
        super("Error: Invalid test scores");
    }

    /**
     * Error with user provided message
     * @param msg the message being provided
     */
    public InvalidTestScore(String msg)
    {
        super("Error: Invalid test scores. " + msg);
    }

    /**
     * Error with test score which caused error.
     * @param amt the test score which errored.
     */
    public InvalidTestScore(double amt)
    {
        super("Error on value " + amt + ": Invalid test Scores.");
    }

    /**
     * Error with user provided message and test score which caused error
     * @param msg the message provided
     * @param amt the test score which errored.
     */
    public InvalidTestScore(String msg, double amt)
    {
        super("Error on value " + amt + ": Invalid test Scores. " + msg);
    }
}