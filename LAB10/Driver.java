/**
 * Demonstrates ExamScores, ExamScoresUpdated, and InvalidTestScore exception extension.
 * 
 * @author Brian J Stout
 */

class Driver
{
    public static void main(String[] args)
    {
        ExamScores examScores1 = new ExamScores(new double[]{90, 20, 10, 75, 13, 82});
        ExamScores examScores2 = new ExamScores(new double[]{90, 20, 10, 75, 13, 82});
        ExamScores examScores3 = new ExamScores(new double[]{80, 20, 10, 75, 13, 82});

        System.out.println("Showing ExamScores.getAverage()");
        System.out.println(examScores1.getAverage());

        System.out.println("\nShowing ExamScores.toString()");
        System.out.println(examScores1);

        System.out.println("\nShowing ExamScores.equals()");
        System.out.println("examScores1 to self : " + examScores1.equals(examScores1));
        System.out.println("examScores1 to null : " + examScores1.equals(null));
        System.out.println("examScores1 to examscores 2 (Equals): " + examScores1.equals(examScores2));
        System.out.println("examScores1 to examscores 3 (Not equals): " + examScores1.equals(examScores3));

        System.out.println("\nShowing ExamScores throws IllegalArgumentException");
        try {
            new ExamScores(new double[]{-90, 20, 10, 120, 13, 82});
        } catch(IllegalArgumentException e) {
            System.out.println(e);
        }

        System.out.println("\nShowing ExamScoresUpdated throws InvalidTestScores");
        try {
            new ExamScoresUpdated(new double[]{120, 30, 40, 60, 90});
        } catch(InvalidTestScore e) {
            System.out.println(e);
        }
    }
}