import java.util.ArrayList;

interface HauntedPlaceComparer
{
    int compare(HauntedPlaces a, HauntedPlaces b);
}

class Sort
{
    public static HauntedPlaceComparer byName = (HauntedPlaces a, HauntedPlaces b) -> a.getName().compareTo(b.getName());
    public static HauntedPlaceComparer byType = (HauntedPlaces a, HauntedPlaces b) -> a.getType().compareTo(b.getType());
    public static HauntedPlaceComparer byPrice = (HauntedPlaces a, HauntedPlaces b) -> Double.compare(a.getPrice(), b.getPrice());
    public static HauntedPlaceComparer byCity = (HauntedPlaces a, HauntedPlaces b) -> a.getLocation().getCity().compareTo(b.getLocation().getCity());
    public static HauntedPlaceComparer byState = (HauntedPlaces a, HauntedPlaces b) -> a.getLocation().getState().compareTo(b.getLocation().getState());

    /**
     * 
     * 
     * @param list
     * @param cmp
     * @return
     */
    public static ArrayList<HauntedPlaces> selectionSort(ArrayList<HauntedPlaces> list, HauntedPlaceComparer cmp)
    {
        for(int i = 0; i < list.size() - 1; i++)
        {
            int min = i;
            for(int j = i + 1; j < list.size(); j++)
            {
                if(cmp.compare(list.get(j), list.get(min)) <= 0)
                {
                    min = j;
                }
            }
            //Swap
            HauntedPlaces temp = list.get(min);
            list.set(min, list.get(i));
            list.set(i, temp);
        }
        return list;
    }
}

