import java.util.Objects;

/**
 * A class representing a "haunted" establishment, uses location instance to establish it's state and city
 */
class HauntedPlaces
{
    private String name;
    private String type;
    private double price;
    private Location location;

    /**
     * Default constructor
     */
    HauntedPlaces()
    {
        this.name = "";
        this.type = "";
        this.price = 0D;
        this.location = null;
    }

    /**
     * Manual constructor
     * @param name the new name
     * @param type the new type
     * @param price the new price
     * @param location the new location
     */
    HauntedPlaces(String name, String type, double price, Location location)
    {
        this.name = name;
        this.type = type;
        this.price = price;
        this.location = location;
    }

    /**
     * Deep Copy constructor
     * @param o the HauntedPlaces object being deep copied
     */
    HauntedPlaces(HauntedPlaces o)
    {
        this.name = new String(o.name);
        this.type = new String(o.type);
        this.price = o.price;
        this.location = new Location(o.location);
    }

    /**
     * Getter for name
     * @return the current name
     */
    public String getName()
    {
        return this.name;
    }

    /**
     * Setter for name
     * @param name the new name
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Getter for type
     * @return the current type
     */
    public String getType()
    {
        return this.type;
    }

    /**
     * Setter for type
     * @param type the new type
     */
    public void setType(String type)
    {
        this.type = type;
    }

    /**
     * Getter for price
     * @return the current price
     */
    public double getPrice()
    {
        return this.price;
    }

    /**
     * Setter for price
     * @param price the new price
     */
    public void setPrice(double price)
    {
        this.price = price;
    }

    /**
     * Getter for location object
     * @return instance of location
     */
    public Location getLocation()
    {
        return this.location;
    }

    /**
     * Setter for location field
     * @param location the new location instance
     */
    public void setLocation(Location location)
    {
        this.location = location;
    }

    /**
     * @param String a string repr of the HauntedPlaces object
     */
    @Override
    public String toString()
    {
        String returnString = "";

        String format = "Name: %20s\t" +
                        "Type: %20s\t" +
                        "Price: $%10.2f\t" +
                        "Location: %20s";
        return returnString.format(format, this.name, this.type, this.price, this.location);
    }

    /**
     * Determines if an haunted places object is equivalent to another one
     * @param O the object being compared to
     */
    @Override
    public boolean equals(Object o)
    {
        if(o == null)
        {
            return false;
        }

        if(o == this)
        {
            return true;
        }

        HauntedPlaces hauntedPlacesObj = (HauntedPlaces)o;
        return (Objects.equals(this.name, hauntedPlacesObj.name) 
            && Objects.equals(this.type, hauntedPlacesObj.type)
            && Objects.equals(this.price, hauntedPlacesObj.price)
            && Objects.equals(this.location, hauntedPlacesObj.location));
    }

    /**
     * Returns an int value determing if "o" is lesser than, greater than, or equal to this instance
     * @param o the instance being compared against
     * @return -1 for lesser than (o is greater than), 0 for equals to, and 1 for greather than
     */
    public int compareTo(HauntedPlaces o)
    {
        int nameComp = this.name.compareTo(o.name);
        if(nameComp != 0)
        {
            return nameComp;
        }
        int typeComp = this.type.compareTo(o.type);
        if(typeComp !=0)
        {
            return typeComp;
        }
        int priceComp = Double.compare(this.price, o.price);
        if(typeComp != 0)
        {
            return typeComp;
        }
        int locationComp = this.location.compareTo(o.location);
        return locationComp;
    }
}

