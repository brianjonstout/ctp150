import java.util.ArrayList;
import java.util.Scanner;
import java.util.Collections;
import java.io.*;

/**
 * The driver that utilizes the HauntedPlaces + Location classes and shows them off.  Reads in a CSV file
 *  and gives options to sort through them.
 * @author Brian J. Stout
 */
class HauntedPlacesDriver
{
    static private final String FILEPATH = "HauntedData.csv";
    static private final int CELLCOUNT = 5;
    static private Scanner fileScan = null;
    static private Scanner IOScan = null;

    public static final String menu =   "1 - Display All Places\n" +
                                        "2 - Search by City Name\n" +
                                        "3 - Search by Type\n" +
                                        "4 - Search by Price\n" +
                                        "5 - Sort by City Name\n" +
                                        "6 - Sort by City Name (desc)\n" +
                                        "7 - Sort by Price\n" +
                                        "8 - Exit\n";
    public static void main(String[] args) throws FileNotFoundException
    {
        openData(HauntedPlacesDriver.FILEPATH);
        ArrayList<HauntedPlaces> hauntedPlacesArray = new ArrayList<HauntedPlaces>();

        while(HauntedPlacesDriver.fileScan.hasNext())
        {
            HauntedPlaces newPlace;
            newPlace = readData();

            if(newPlace != null)
            {
                hauntedPlacesArray.add(newPlace);
            }
        }

        closeData();
        mainMenu(hauntedPlacesArray);
        HauntedPlacesDriver.IOScan.close();
    }

    static public void openData(String path) throws FileNotFoundException
    {
        File file = new File(path);

        if(!file.exists() || !file.isFile())
        {
            System.out.println("File: " + file + " does not exist, or is not a file");
            System.exit(0);
        }

        HauntedPlacesDriver.fileScan = new Scanner(file);
        HauntedPlacesDriver.fileScan.nextLine(); // Skip first line to avoid errors
    }

    /**
     * Reads the CSV file one line at a time
     * @return HauntedPlaces object
     */
    static public HauntedPlaces readData()
    {
        if(HauntedPlacesDriver.fileScan.hasNext() == false)
        {
            System.out.println("ERROR: readData() called on empty file");
            return null;
        }

        HauntedPlaces returnPlace = null;
        Location newLocation = null;

        //Reads the next line of the file and splits it by the commas for each cell
        String record = HauntedPlacesDriver.fileScan.nextLine();
        String[] cell = record.split(",");

        //String.split won't add empty data so ,,,,,,, will return a length of 0
        if(cell.length == 0)
        {
            return returnPlace;
        }

        // From the format provided, 1840 seems to be an end indicator, so we just quit here
        if(cell.length == 8 && cell[7].equals("1840"))
        {
            return returnPlace;
        }

        // Indicates a improperly formatted cell (missing value most likely)
        if(cell.length != CELLCOUNT)
        {
            System.out.println("Cellcount " + cell.length);
            System.out.println( "Error: input file constains improperly formatted cells.\n Row: \"" + record +
                                "\" should contain 8 (3 blank) items separated by 7 commas.  Exiting...");
            System.exit(0);
        }

        newLocation = new Location(cell[3], cell[4]);
        returnPlace = new HauntedPlaces(cell[0], cell[1], parseInputForDouble(cell[2]), newLocation);
        
        return returnPlace;

    }

    /**
     * Closes the scan handle after the CSV file is completely read
     */
    static public void closeData()
    {
        HauntedPlacesDriver.fileScan.close();
    }

    /**
     * Loops through all the times in the Arraylist and prints them
     * @param list list being printed
     */
    static public void displayAllPlaces(ArrayList<HauntedPlaces> list)
    {
        for(HauntedPlaces i : list)
        {
            System.out.println(i);
        }
    }

    /**
     * The main loop of the driver, has the option to select all sub-menus and exit
     * @param hauntedPlacesArray
     */
    static public void mainMenu(ArrayList<HauntedPlaces> hauntedPlacesArray)
    {
        while(true)
        {
            System.out.print(menu);
            System.out.print("Enter Choice: ");

            HauntedPlacesDriver.IOScan = new Scanner(System.in);
            String input = "";
            input = HauntedPlacesDriver.IOScan.nextLine();

            // Inputted string should only be one character
            if(input.length() != 1)
            {
                input = "0";
            }
    
            switch(input.charAt(0))
            {
                case '1':
                    System.out.println();
                    displayAllPlaces(hauntedPlacesArray);
                    break;
                case '2':
                    System.out.println();
                    searchByCityMenu(hauntedPlacesArray);
                    break;
                case '3':
                    System.out.println();
                    searchByTypeMenu(hauntedPlacesArray);
                    break;
                case '4':
                    System.out.println();
                    searchByPriceMenu(hauntedPlacesArray);
                    break;
                case '5':
                    System.out.println();
                    Sort.selectionSort(hauntedPlacesArray, Sort.byCity);
                    displayAllPlaces(hauntedPlacesArray);
                    break;
                case '6':
                    System.out.println();
                    Sort.selectionSort(hauntedPlacesArray, Sort.byCity);
                    Collections.reverse(hauntedPlacesArray);
                    displayAllPlaces(hauntedPlacesArray);
                    break;
                case '7':
                    System.out.println();
                    Sort.selectionSort(hauntedPlacesArray, Sort.byPrice);
                    displayAllPlaces(hauntedPlacesArray);
                    break;
                case '8':
                    System.out.println();
                    HauntedPlacesDriver.IOScan.close();
                    System.exit(0);
                    break;
                default:
                    System.out.println("Not a valid option\n");
                    continue;
            }
        }
    }

    /**
     * Jumps into the menu selection for searching by the city, prompts the user for city name
     * @param hauntedPlacesArray the ArrayList to be passed to searchByCity()
     */
    static public void searchByCityMenu(ArrayList<HauntedPlaces> hauntedPlacesArray)
    {
        System.out.print("Enter city name: ");
        String input = "";
        input = HauntedPlacesDriver.IOScan.nextLine();

        ArrayList<HauntedPlaces> list = searchByCity(input, hauntedPlacesArray);
        if(list.isEmpty())
        {
            System.out.println("No Haunted Places are in the city " + input);
        }
        else
        {
            System.out.println();
            Sort.selectionSort(list, Sort.byCity);
            displayAllPlaces(list);
        }
    }

    /**
     * Returns a list of all HauntedPlaces that match a provided city name
     * @param city the city being searched for
     * @param list the list being searched through
     * @return A shallow copy list of all the HauntedPlaces instances that contain the same city, sorted by city name
     */
    static public ArrayList<HauntedPlaces> searchByCity(String city, ArrayList<HauntedPlaces> list)
    {
        ArrayList<HauntedPlaces> matchList = new ArrayList<HauntedPlaces>();

        for(HauntedPlaces i : list)
        {
            if(city.equalsIgnoreCase(i.getLocation().getCity()))
            {
                matchList.add(i);
            }
        }
        return matchList;
    }

    /**
     * Jumps into the menu selection for searching by the type, prompts the user for type name
     * @param hauntedPlacesArray the ArrayList to be passed to
     */
    static public void searchByTypeMenu(ArrayList<HauntedPlaces> hauntedPlacesArray)
    {
        System.out.print("Enter type name: ");
        String input = "";
        input = HauntedPlacesDriver.IOScan.nextLine();

        ArrayList<HauntedPlaces> list = searchByType(input, hauntedPlacesArray);
        if(list.isEmpty())
        {
            System.out.println("No Haunted Places are of the type " + input);
        }
        else
        {
            System.out.println();
            Sort.selectionSort(list, Sort.byType);
            displayAllPlaces(list);
        }
    }

    /**
     * Returns a list of all HauntedPlaces that match the provided Type
     * @param type the type being searched for
     * @param list the list being searched through
     * @return A shallow copy list of all the Haunted Places instances that contain the same type, sorted by type
     */
    static public ArrayList<HauntedPlaces> searchByType(String type, ArrayList<HauntedPlaces> list)
    {
        ArrayList<HauntedPlaces> matchList = new ArrayList<HauntedPlaces>();

        for(HauntedPlaces i : list)
        {
            if(type.equalsIgnoreCase(i.getType()))
            {
                matchList.add(i);
            }
        }
        return matchList; 
    }

    /**
     * Jumps into the menu selection for searching by price, a user will be prompted for their serach qualification
     *  (less than, greater than, equal too) and then prompted for a price comparison point
     * @param hauntedPlacesArray the array being searched
     */
    static public void searchByPriceMenu(ArrayList<HauntedPlaces> hauntedPlacesArray)
    {
        System.out.print(   "1. Less Than or equal to\n" +
                            "2. Greater Than or equal to\n" +
                            "3. Equal To\n"
        );
        String input = "";
        input = HauntedPlacesDriver.IOScan.nextLine();

        int indicator = 0;
    
        switch(input.charAt(0))
        {
            case '1':
                indicator = -1;
                break;
            case '2':
                indicator = 1;
                break;
            case '3':
                indicator = 0;
                break;
            default:
                System.out.println("Not a valid input, going back to main menu\n");
                return;
        }
        System.out.println("Enter your price: ");
        input = HauntedPlacesDriver.IOScan.nextLine();

        ArrayList<HauntedPlaces> list = searchByPrice(indicator, parseInputForDouble(input), hauntedPlacesArray);

        if(list.isEmpty())
        {
            System.out.println("There were no entry at price point: " + input + " or the list is empty");
        }
        else
        {
            System.out.println();
            Sort.selectionSort(list, Sort.byPrice);
            displayAllPlaces(list);
        }
    }

    /**
     * Searches through a list, and adds any instances that meet it's price comparison criteria
     * @param indicator whether the price is compared to greater than (or eq), less than (or eq), or equal to
     * @param priceCmp the price point being compared to
     * @param list the list being searched through
     * @return a list (sorted by price) of all the items that meet the user's criteria
     */
    static public ArrayList<HauntedPlaces> searchByPrice(int indicator, double priceCmp, ArrayList<HauntedPlaces> list)
    {
        ArrayList<HauntedPlaces> matchList = new ArrayList<HauntedPlaces>();
        
        if(indicator == 0)
        {
            for(HauntedPlaces i : list)
            {
                if(priceCmp == i.getPrice())
                {
                    matchList.add(i);
                }
            }
        }
        else if(indicator == -1)
        {
            for(HauntedPlaces i : list)
            {
                if(i.getPrice() <= priceCmp)
                {
                    matchList.add(i);
                }
            }
        }
        else
        {
            for(HauntedPlaces i : list)
            {
                if(i.getPrice() >= priceCmp)
                {
                    matchList.add(i);
                }
            } 
        }
        return matchList; 
    }

    /**
     * Given a string it will attempt to convert that string to a number using Double.parseDouble().  If .parseDouble()
     * throws a NumberFormatException it will return a null.
     * 
     * @param userInput the string parsed.
     * @return A converted Double from a string, or null.
     */
    public static Double parseInputForDouble(String userInput)
    {
        Double returnDouble = 0.0;

        try {
            returnDouble = Double.parseDouble(userInput);
        } catch (Exception e) {
            System.out.println( "Error: input file constains improperly formatted cells.\nprice: \"" + userInput +
                                "\" could not be converted to a number.  Exiting...");
            System.exit(0);
        }   
        return returnDouble;
    }
}