import java.util.Objects;

/**
 * A location object provides a location's city and state
 */
class Location {
    private String city;
    private String state;

    /**
     * Default constructor
     */
    Location()
    {
        this.city = "";
        this.state = "";
    }

    /**
     * Manual constructor
     * @param city the city
     * @param state the state
     */
    Location(String city, String state)
    {
        this.city = city;
        this.state = state;
    }

    /**
     * Deep copy constructor
     * @param o the Location obj being copied
     */
    Location(Location o)
    {
        this.city = new String(o.city);
        this.state = new String(o.state);
    }

    /**
     * Getter for city
     * @return the city name
     */
    public String getCity()
    {
        return this.city;
    }

    /**
     * Setter for city
     * @param city what the new city name will be
     */
    public void setCity(String city)
    {
        this.city = city;
    }

    /**
     * getter for state
     * @return state
     */
    public String getState()
    {
        return this.state;
    }

    /**
     * setter for state
     * @param state new state
     */
    public void setState(String state)
    {
        this.state = state;
    }

    /**
     * @return String repr of location object
     */
    @Override
    public String toString()
    {
        String returnString = "";
        String format = "%s, %s";
        return returnString.format(format, this.city, this.getStateInitials());
    }

    /**
     * @return boolean of if an object is equals or not
     */
    @Override
    public boolean equals(Object o)
    {

        if(o == null) {
            return false;
        }

        if(o == this) {
            return true;
        }

        Location locationObj = (Location)o;
        return (Objects.equals(this.city, locationObj.city)
            && Objects.equals(this.state, locationObj.state));
    }

    /**
     * compareTo implementation for Location
     * @param o
     * @return
     */
    public int compareTo(Location o)
    {
        int firstComp = this.city.compareTo(o.city);
        if (firstComp != 0)
        {
            return firstComp;
        }
        return this.state.compareTo(o.state);
    }

    /**
     * Returns the state initial representation of the current state
     * @return the state initials
     */
    public String getStateInitials()
    {

        //TODO: I imagine this is incredibly slow for Wyoming, swould switch this to a dictionary
        //        or a hash map
        switch(this.state)
        {
            case "Alabama":
                return "AL";
            case "Alaska":
                return "AK";
            case "Arizona":
                return "AZ";
            case "Arkansas":
                return "AR";
            case "California":
                return "CA";
            case "Colorado":
                return "CO";
            case "Connecticut":
                return "CT";
            case "Delaware":
                return "DE";
            case "Florida":
                return "FL";
            case "Georgia":
                return "GA";
            case "Hawaii":
                return "HI";
            case "Idaho":
                return "ID";
            case "Illinois":
                return "IL";
            case "Indiana":
                return "IN";
            case "Iowa":
                return "IA";
            case "Kansas":
                return "KS";
            case "Kentucky":
                return "KY";
            case "Louisiana":
                return "LA";
            case "Maine":
                return "ME";
            case "Maryland":
                return "MD";
            case "Massachusetts":
                return "MA";
            case "Michigan":
                return "MI";
            case "Minnesota":
                return "MN";
            case "Mississippi":
                return "MS";
            case "Missouri":
                return "MO";
            case "Montana" : 
                return "MT";
            case "Nebraska":
                return "NE";
            case "Nevada":
                return "NV";
            case "New Hampshire":
                return "NH";
            case "New Jersey":
                return "NJ";
            case "New Mexico":
                return "NM";
            case "New York":
                return "NY";
            case "North Carolina":
                return "NC";
            case "North Dakota":
                return "ND";
            case "Ohio":
                return "OH";
            case "Oklahoma":
                return "OK";
            case "Oregon":
                return "OR";
            case "Pennsylvania":
                return "PA";
            case "Rhode Island":
                return "RI";
            case "South Carolina":
                return "SC";
            case "South Dakota":
                return "SD";
            case "Tennessee":
                return "TN";
            case "Texas":
                return "TX";
            case "Utah":
                return "UT";
            case "Vermont":
                return "VT";
            case "Virginia":
                return "VA";
            case "Washington":
                return "WA";
            case "West Virginia":
                return "WV";
            case "Wisconsin":
                return "WI";
            case "Wyoming":
                return "WY";
            default:
                return null;
        }
    }
}