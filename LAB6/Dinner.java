import java.util.Scanner;
import java.io.*;
import java.util.Objects;

class Dinner
{
    private String appName;
    private Double appPrice;
    private String entreeName;
    private Double entreePrice;

    /**
     * Default constructor for Dinner, give back a Dinner object with empty strings and 0 Value doubles
     */
    Dinner()
    {
        this.appName = "";
        this.appPrice = 0D;
        this.entreeName = "";
        this.entreePrice = 0D;
    }

    /**
     * Constructor for Dinner where you provide the assigned values
     * 
     * @param appName the name of the appetizer
     * @param appPrice the price of the appetizer
     * @param entreeName the name of the entree
     * @param entreePrice the price of the entree
     */
    Dinner(String appName, Double appPrice, String entreeName, Double entreePrice)
    {
        this.appName = appName;
        this.appPrice = appPrice;
        this.entreeName = entreeName;
        this.entreePrice = entreePrice;
    }

    /**
     * Setter for appName
     * 
     * @param appName what the new appName will be called
     */
    public void setAppName(String appName)
    {
        this.appName = appName;
    }

    /**
     * Getter for app name
     * 
     * @return the current app name
     */
    public String getAppName()
    {
        return this.appName;
    }

    /**
     * The setter for the app price
     * 
     * @param appPrice the new app price
     */
    public void setAppPrice(Double appPrice)
    {
        this.appPrice = appPrice;
    }

    /**
     * The getter for the app price
     * 
     * @return the current app price
     */
    public Double getAppPrice()
    {
        return this.appPrice;
    }

    /**
     * The setter for the entree name
     * 
     * @param entreeName the new entree name
     */
    public void setEntreeName(String entreeName)
    {
        this.entreeName = entreeName;
    }

    /**
     * The getter for the entree name
     * 
     * @return the current entree name
     */
    public String getEntreeName()
    {
        return this.entreeName;
    }

    /**
     * The setter for the entree Price
     * 
     * @param entreePrice the new entree price
     */
    public void setEntreePrice(Double entreePrice)
    {
        this.entreePrice = entreePrice;
    }

    /**
     * The getter for the entree price
     * 
     * @return the current entree price
     */
    public Double getEntreePrice()
    {
        return this.entreePrice;
    }

    /**
     * Returns a formatted string of the current Appetizer and Entree, names, and prices
     */
    public String toString()
    {
        String returnString = "";

        String format = "The Appetizer is: %s\n" +
                        "The price of the Appetizer is: $%.2f\n" +
                        "Entree is: %s\n" +
                        "The price of the entree is: $%.2f";

        returnString = String.format(format, getAppName(), getAppPrice(), getEntreeName(), getEntreePrice());
        return returnString;
    }

    //CREDIT: https://www.sitepoint.com/implement-javas-equals-method-correctly/
    /**
     * Determines if the data in each object is equivalent
     */
    @Override
    public boolean equals(Object o)
    {
        // self check
        if (this == o)
            return true;

        // null check
        if (o == null)
            return false;

        // type check and cast
        if (getClass() != o.getClass())
            return false;

        Dinner dinner = (Dinner) o;

        // field comparison
        return Objects.equals(this.appName, dinner.appName)
                && Objects.equals(this.appPrice, dinner.appPrice)
                && Objects.equals(this.entreeName, dinner.entreeName)
                && Objects.equals(this.entreePrice, dinner.entreePrice);
    }
}