import java.util.ArrayList;
import java.util.Scanner;
import java.io.*;

/**
 * Quick program that demonstrates the utility of the Dinner class provided in the Dinner.java file
 * @author Brian J Stout
 */
class DinnerDriver
{

    static private final String FILEPATH = "restaurant.csv";
    static private String dinerName = "";
    static private Scanner scan = null;

    static public void main(String[] args) throws FileNotFoundException
    {
        openData(DinnerDriver.FILEPATH);

        ArrayList<Dinner> dinnerArray = new ArrayList<Dinner>();
        while(DinnerDriver.scan.hasNext())
        {
            dinnerArray.add(readData());
        }

        System.out.println("The resturaunt is: " + DinnerDriver.dinerName + "\n");

        //  Loops through each object in dinnerArray and prints it using the Dinner.tostring() method
        for (Dinner i : dinnerArray)
        {
            System.out.println(i + "\n");
        }

        printTotal(dinnerArray);
        closeData();
    }

    /**
     * Checks to see if the file provided in path exists, opens up a Scanner assigning it to the scan global
     *   and then reads the name of the resturaunt
     * 
     * @param path the file path to the resturaunt.csv file
     * @throws FileNotFoundException
     */
    static public void openData(String path) throws FileNotFoundException
    {
        File file = new File(path);

        if(!file.exists() || !file.isFile())
        {
            System.out.println("File: " + file + " does not exist, or is not a file");
            System.exit(0);
        }

        DinnerDriver.scan = new Scanner(file);
        DinnerDriver.dinerName = DinnerDriver.scan.nextLine();
        DinnerDriver.dinerName = DinnerDriver.dinerName.split(",")[0];  //Grab the first line before the first comma.
    }

    /**
     * Reads a single line of the csv file which should look like appetizer name, appetizer price, entree name, entree price
     *   also does some basic error checking and will exit the program if the CSV file isn't formatted correctly
     * 
     * @return a newly constructed instance of the Dinner class
     */
    static public Dinner readData()
    {
        if(DinnerDriver.scan.hasNext() == false)
        {
            System.out.println("ERROR: readData() called on empty file");
            return null;
        }

        Dinner returnDinner = null;

        //Reads the next line of the file and splits it by the commas for each cell
        String record = DinnerDriver.scan.nextLine();
        String[] cell = record.split(",");

        if(cell.length != 4)
        {
            System.out.println( "Error: input file constains improperly formatted cells.\n Row: \"" + record +
                                "\" should contain 4 items separated by 3 commas.  Exiting...");
            System.exit(0);
        }

        returnDinner = new Dinner(cell[0], ParseInputForDouble(cell[1]), cell[2], ParseInputForDouble(cell[3]));

        return returnDinner;
    }

    /**
     * Runs the program's clean up operation to close graciously
     */
    static public void closeData()
    {
        DinnerDriver.scan.close();
    }

    /**
     * Loops through each item in the Dinner ArrayList and prints out the names of the dinner's appetizer and entree
     *   as well as the total price of each dinner
     * 
     * @param dinnerArray an ArrayList of Dinner objects
     */
    static public void printTotal(ArrayList<Dinner> dinnerArray)
    {
        System.out.println("The totals of the dinners are:");
        int count = 1;

        for (Dinner i : dinnerArray)
        {
            Double dinnerTotal = i.getAppPrice() + i.getEntreePrice();
            System.out.println("\t" + count + ". The " + i.getAppName() + " and " + i.getEntreeName() + ": " + String.format("$%.2f\n", dinnerTotal));
            count++;
        }

    }

    /**
     * Given a string it will attempt to convert that string to a number using Double.parseDouble().  If .parseDouble()
     * throws a NumberFormatException it will return a null.
     * 
     * @param userInput the string parsed.
     * @return A converted Double from a string, or null.
     */
    public static Double ParseInputForDouble(String userInput)
    {
        Double returnDouble = 0.0;

        try {
            returnDouble = Double.parseDouble(userInput);
        } catch (Exception e) {
            System.out.println( "Error: input file constains improperly formatted cells.\nAppetizer or Entree price: \"" + userInput +
                                "\" could not be converted to a number.  Exiting...");
            System.exit(0);
        }   
        return returnDouble;
    }
}